<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StripeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\HomeController::class,'index']);
Route::get('/add-product', [App\Http\Controllers\ProductController::class, 'create'])->name('add_product');
Route::get('/edit-product/{id}', [App\Http\Controllers\ProductController::class, 'edit'])->name('edit_product');
Route::post('/save-product', [App\Http\Controllers\ProductController::class, 'store'])->name('save_product');
Route::put('/update-product/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('update_product');
Route::DELETE('/delete-product/{id}', [App\Http\Controllers\ProductController::class, 'destroy']);


Route::get('checkout',[CartController::class,'checkoutView']);

// Front End routes

Route::get('products',[ProductController::class,'frontendProducts']);
Route::post('add-to-cart',[CartController::class,'AddToCart']);
Route::post('update-to-cart',[CartController::class,'UpdateToCart']);
Route::post('remove-from-cart',[CartController::class,'RemoveFromCart']);

Route::get('cart',[CartController::class,'cartView']);



//Auth Routesss
Route::get('login',[AuthController::class,'loginView'])->name('login');

Route::get('register',[AuthController::class,'registerView'])->name('register');
Route::post('save-user',[AuthController::class,'saveUser'])->name('saveUser');
Route::post('make-login',[AuthController::class,'login'])->name('makeLogin');
Route::post('save-order',[OrderController::class,'saveOrder']);
Route::group(['middleware'=>['auth']],function (){


    Route::get('logout',[AuthController::class,'logout'])->name('logout');
    Route::get('payment/{id}',[PaymentController::class,'paymentView']);
    Route::post('make-payment',[PaymentController::class,'store']);

});
Route::get('/stripe-payment/{id}', [StripeController::class, 'handleGet']);
    Route::post('/stripe-payment', [StripeController::class, 'handlePost'])->name('stripe.payment');

    Route::get('call-command',function (){
       \Illuminate\Support\Facades\Artisan::call('optimize:clear');
       dd('done');
    });

    Route::get('testing',function (){

//        $users = \App\Models\User::ComMailUsers()->limit(10)->get();
        dump(app()->make('CustomUser'));
//        dd(app()->make('CustomUser'));
        dd('service provider');
    });
