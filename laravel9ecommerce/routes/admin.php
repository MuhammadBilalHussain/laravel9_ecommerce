<?php

use App\Http\Controllers\Admin\AdminGeneralController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Route;



Route::get('admin/login',function (){
    return view('admin.auth.login');
});
Route::post('admin-login',[AuthController::class,'adminLogin']);

Route::group(['prefix'=>'admin', 'middleware'=>'auth:admin'],function(){
    Route::get('/dashboard', function () {
        return view('welcome');
    });
    Route::get('products', [App\Http\Controllers\ProductController::class, 'index'])->name('products_list');
    Route::get('payment-verification', [PaymentController::class, 'PaymentList'])->name('payment_list');
    Route::get('verify-payment/{id}', [PaymentController::class, 'verifyPayment']);
    Route::get('decline-payment/{id}', [PaymentController::class, 'declinePayment']);
    Route::get('invoice/{id}', [AdminGeneralController::class, 'invoicePrint']);
    Route::get('make-AFS/{id}', [AdminGeneralController::class, 'makeAFS']);

    Route::get('/wfp',[PaymentController::class,'wfpView'])->name('wfp');
    Route::get('/rfa',[PaymentController::class,'rfaView'])->name('rfa');
    Route::get('/afs',[PaymentController::class,'afsView'])->name('afs');
    Route::get('/del',[PaymentController::class,'delView'])->name('del');
    Route::get('/office-pickups',[GeneralController::class,'OfficePickups'])->name('OfficePickups');
    Route::get('mark-as-paid/{id}',[GeneralController::class,'MarkAsPaid']);

});

