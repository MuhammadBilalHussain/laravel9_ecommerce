<?php
namespace App\interfaces;


interface ProductInterface {
    public function allProduct();
    public function storeProduct($request);
    public function editProduct($id);
    public function updateProduct($request,$id);
    public function deleteProduct($id);
}
