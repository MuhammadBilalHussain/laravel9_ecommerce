<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\ServiceProvider;

class TestSericeProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('CustomUser',function (){
            return User::find(1);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
