<?php
namespace App\Services;

use App\Models\Order;
use App\Models\payment;
use App\Models\PaymentTransaction;
use App\Models\PaymentVerification;

class PaymentService{
    public function OrderPayment($order, $data){
        if ($order->delivery_method == 'Office Pickup' || $order->payment_method == 3){
            $orderStatus = 'DEL';
        } else {
            $orderStatus = 'RFA';
        }

        if ($order->payment_method == 2){
            PaymentTransaction::create([
                'order_id'=>$order->id,
                'payment_method'=>$order->payment_method,
                'user_id'=>auth()->id(),
                'date'=>now(),
                'total'=>$order->total,
//            'proof'=>$imageName,
//                'sender_name'=>$request->sender_name,
//                'paid_amount'=>$request->paid_amount,
//                'sender_note'=>$request->sender_note,
                'order_status'=>'RFA',
//                'ref_no'=>$request->ref_no
            ]);
        }else{
            $paymentVerification = PaymentVerification::where('order_id',$order->id)->first();
            PaymentTransaction::create([
                'order_id'=>$order->id,
                'payment_method'=>$order->payment_method,
                'user_id'=>auth()->id(),
                'date'=>now(),
                'total'=>$order->total,
                'proof'=>$paymentVerification->proof,
                'sender_name'=>$paymentVerification->sender_name,
                'paid_amount'=>$paymentVerification->paid_amount,
                'sender_note'=>$paymentVerification->sender_note,
                'order_status'=>'RFA',
                'ref_no'=>$paymentVerification->ref_no
            ]);
        }




        Order::where('id',$order->id)->update([
            'order_status'=> $orderStatus
        ]);

        return true;
    }


}
