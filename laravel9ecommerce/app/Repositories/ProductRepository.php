<?php

namespace App\Repositories;

use App\interfaces\ProductInterface;
use App\Models\Product;

class ProductRepository implements ProductInterface {


    public function allProduct()
    {
        $products = Product::all();
        return $products;
    }

    public function storeProduct($request)
    {
        $imageName=null;
        if ($request->hasFile('product_image')) {
            $image = $request->file('product_image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/product_images');
            $image->move($destinationPath, $imageName);
        }


       $product =  Product::create([
            'product_name'=>$request->product_name,
            'price'=>$request->product_price,
            'quantity'=>$request->product_quantity,
            'product_weight' =>$request->product_weight,
            'product_type' =>$request->product_type,
            'image'=>$imageName,
        ]);
        return $product;
    }

    public function editProduct($id)
    {
        return Product::where('id',$id)->first();
    }

    public function updateProduct($request, $id)
    {
        $imageName=null;
        if ($request->hasFile('product_image')) {
            $image = $request->file('product_image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/product_images');
            $image->move($destinationPath, $imageName);
        }

        Product::where('id',$id)->update([
            'product_name'=>$request->product_name,
            'price'=>$request->product_price,
            'quantity'=>$request->product_quantity,
            'product_weight' =>$request->product_weight,
            'product_type' =>$request->product_type,
            'image'=>$imageName,
        ]);
        return true;
    }

    public function deleteProduct($id)
    {
        Product::where('id',$id)->delete();
        return true;
    }
}
