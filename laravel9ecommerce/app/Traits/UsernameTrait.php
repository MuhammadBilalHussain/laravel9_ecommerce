<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UsernameTrait
{
    public function Username($first_name, $last_name){
        $randomNumber = Str::random();
        return $first_name.substr($last_name,1,3).$randomNumber;
    }
}
