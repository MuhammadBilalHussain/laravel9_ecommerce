<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function PaymentMethod(){
        return $this->belongsTo(PaymentMethod::class,'payment_method','id');
    }
    public function OrderPayment(){
        return $this->belongsTo(payment::class,'id','order_id');
    }


    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function BillingInfo(){
        return $this->belongsTo(BillingDetail::class,'billing_detail_id','id');
    }
    public function OrderItems(){
        return $this->hasMany(OrderItem::class,'order_id','id');
    }}
