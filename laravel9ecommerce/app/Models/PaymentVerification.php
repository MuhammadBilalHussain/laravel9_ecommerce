<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentVerification extends Model
{
    use HasFactory;
    protected $guarded=[];


    public function PaymentMethod(){
        return $this->belongsTo(PaymentMethod::class,'payment_method','id');
    }
    public function Order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }

}
