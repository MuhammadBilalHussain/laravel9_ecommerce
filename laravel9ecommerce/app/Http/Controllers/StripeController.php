<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\payment;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;


class StripeController extends Controller
{
    public $paymentService;
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }
    public function handleGet($orderID){
        $id = Crypt::decrypt($orderID);
        $order = Order::where('id',$id)->first();

        return view('frontend.stripe.index',compact('order'));
    }

    public function handlePost(Request $request){
        $order = Order::with(['User','OrderItems','BillingInfo'])->where('id', $request->order_id)->first();

        Stripe::setApiKey(env('STRIPE_SECRET'));
        $charge =  Charge::create ([
            "amount" => $order->total * 150,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "New payment.",
            "receipt_email"=>"test@gmail.com",
//            "customer" => $order->user,
            "shipping" => [
                "name" => $order->User->name,
                "address" => [
                    "line1" => $order->BillingInfo->address_one,
                    "postal_code" => "",
                    "city" => $order->BillingInfo->city,
                    "state" => $order->BillingInfo->province,
                    "country" => '',
                ],

            ]
        ]);
        if(isset($charge->id) && $charge->id != ''){
            $this->paymentService->OrderPayment($order, $charge);
            Session::flash('success', 'Payment has been successfully processed.');
            return back();
        } else {
            Session::flash('success', 'Something went wrong');
            return back();
        }
    }
}
