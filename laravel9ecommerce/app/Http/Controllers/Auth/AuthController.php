<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\UsernameTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{    use UsernameTrait;

    public function loginView(){
        return view('Auth.login');
    }
    public function registerView(){
        return view('Auth.register');
    }
    public function saveUser(Request $request){
        $this->validate($request,[
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email|unique:users,email',
            'phone'=>'required',
            'password'=>'required'

        ]);
        $username = $this->Username($request->first_name,$request->last_name);

        $user = User::create([

//            'first_name'=>$request->first_name,
//            'last_name'=>$request->last_name,
            'name' => $request->first_name,
            'username'=>$username,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>Hash::make($request->password),
        ]);
        Auth::loginUsingId($user->id);
        return redirect('/');

    }
    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

        $arr = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(Auth::attempt($arr)){
            return redirect('/')->with(['success'=>true, 'message' =>'Login Successfully']);
        }else{
            return back()->with(['error'=>'Invalid Credentials']);
        }
    }
    public function logout(){
        Auth::logout();

        return redirect('login')->with(['success'=>'Logout successfully']);
    }

    public function adminLogin(Request $request){
//        dd($request);
        if (Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password])) {
            return redirect('admin/dashboard');
        }else{
            return back()->with(['error'=>'invalid credentials']);
        }
    }
}
