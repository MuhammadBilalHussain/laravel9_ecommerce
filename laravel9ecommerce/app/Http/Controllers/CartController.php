<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function AddToCart(Request $request)
    {
//        dd($request->all());
                  //pID                pName                  Qty        Price              weight               other options
        Cart::add($request->product_id,$request->product_name,1,$request->product_price,$request->product_weight,[
            'image'=>$request->image,
            'test'=>'abc'
        ]);

        return response()->json(['data'=>null,'message'=>'Add to cart successfully','cart_items' => Cart::count()]);
        dd($request,Cart::Content());
    }

    public function cartView(){
        $cart = Cart::content();
        return view('frontend.cart',compact('cart'));
    }
    public function UpdateToCart(Request $request){
        Cart::update($request->cart_id,$request->qty);
        return response()->json(['data'=>null,'message'=>'Cart updated Successfully','cart_items' => Cart::count()]);
    }
    public function checkoutView(){
        $mop = PaymentMethod::all();
        $cart = Cart::content();

        return view('frontend.checkout',compact('cart','mop'));
    }
    public function RemoveFromCart(Request $request){
        Cart::remove($request->cart_id);

        return response()->json(['data'=>null,'message'=>'Cart updated Successfully','cart_items' => Cart::count()]);

    }



}
