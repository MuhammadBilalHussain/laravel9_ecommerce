<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\payment;
use App\Models\PaymentVerification;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function PaymentList(){
        $payments = PaymentVerification::where('status','pending')->get();
        $payments_approved = PaymentVerification::where('status','approve')->get();
        $payments_decline = PaymentVerification::where('status','decline')->get();

        return view('admin.payment_list',compact('payments','payments_approved','payments_decline'));
    }

    public function verifyPayment($id){
        $ID = decrypt($id);
        $payment = PaymentVerification::where('id',$ID)->first();

        $payment->update([
           'status'=>'approve',
           'verification_date'=>now(),
        ]);

        Order::where('id',$payment->order_id)->update([
           'order_status' => 'RFA'
        ]);


        return back()->with(['success'=>'Payment Verified successfully']);
    }
    public function declinePayment($id){
        $ID = decrypt($id);
        PaymentVerification::where('id',$ID)->update([
            'status'=>'decline',
        ]);

        return back()->with(['success'=>'Payment Declined successfully']);
    }

    public function wfpView(){
        $orders = Order::where('order_status','WFP')->where('delivery_method','!=','Office pickup')->get();
        return view('admin.orderFulfillment.wfp',compact('orders'));

    }
    public function rfaView(){
        $orders = Order::with(['OrderPayment'])->where('order_status','RFA')->get();
//        dd($orders);
        return view('admin.orderFulfillment.rfa',compact('orders'));

    }
    public function afsView(){
        $orders = Order::where('order_status','AFS')->get();
        return view('admin.orderFulfillment.afs',compact('orders'));

    }
    public function delView(){
        $orders = Order::where('order_status','DEL')->get();
        return view('admin.orderFulfillment.del',compact('orders'));
    }
}
