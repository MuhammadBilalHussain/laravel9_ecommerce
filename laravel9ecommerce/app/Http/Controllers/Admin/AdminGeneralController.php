<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class AdminGeneralController extends Controller
{
    public function invoicePrint($id){
        $order = Order::with(['OrderItems.Product','BillingInfo','User','PaymentMethod'])->where('id',$id)->first();

        return view('admin.invoice.index',compact('order'));
    }

    public function makeAFS($id){
        Order::where('id',$id)->update([
           'order_status'=>'AFS'
        ]);

        return redirect('admin/afs')->with(['success'=>'Order Added to AFS Successfully']);
    }
}
