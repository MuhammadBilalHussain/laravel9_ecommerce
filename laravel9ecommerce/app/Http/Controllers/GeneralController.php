<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Services\PaymentService;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public $paymentService;
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function OfficePickups(){
        $orders = Order::with(['OrderPayment'])->where('delivery_method','Office Pickup')->get();
        return view('admin.orderFulfillment.office-pickup',compact('orders'));
    }


    public function MarkAsPaid($id){
        $order = Order::where('id',decrypt($id))->first();
        $otherData = (object)[
          'description' => 'Paid on counter',
          'id'=> $order->id
        ];
        $this->paymentService->OrderPayment($order, $otherData);
        return back()->with(['success'=>'Paid Successfully']);
    }
}
