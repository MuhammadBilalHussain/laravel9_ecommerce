<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\payment;
use App\Models\PaymentMethod;
use App\Models\PaymentTransaction;
use App\Models\PaymentVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class PaymentController extends Controller
{
    public function paymentView($orderID)
    {
        $id = Crypt::decrypt($orderID);
        $order = Order::where('id',$id)->first();
        $mop = PaymentMethod::whereNotIn('id',[3,4])->get();
        return view('frontend.payment',compact('order','mop'));
    }
    public function store(Request $request){
        $imageName=null;
        if ($request->hasFile('proof')) {
            $image = $request->file('proof');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/payment_proofs');
            $image->move($destinationPath, $imageName);
        }




        PaymentVerification::create([
            'order_id'=>$request->order_id,
            'payment_method'=>$request->payment_method,
            'user_id'=>auth()->id(),
            'date'=>now(),
            'total'=>$request->total,
            'proof'=>$imageName,
            'sender_name'=>$request->sender_name,
            'paid_amount'=>$request->paid_amount,
            'sender_note'=>$request->sender_note,
            'ref_no'=>$request->ref_no
        ]);

        Order::where('id',$request->order_id)->update([
           'order_status'=>'RFA'
        ]);

        return redirect('/')->with(['success'=>'Payment done successfully']);
    }
}
