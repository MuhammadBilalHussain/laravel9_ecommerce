<?php

namespace App\Http\Controllers;

use App\Models\BillingDetail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Traits\UsernameTrait;
//use Gloudemans\Shoppingcart\Cart;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class OrderController extends Controller
{
    use UsernameTrait;
    public function saveOrder(Request $request){




//        dd($request);
//        $this->validate($request,[
//           'first_name'=>'required',
//           'last_name'=>'required',
//           'email'=>'required',
//           'phone'=>'required',
//           'province'=>'required',
//           'city'=>'required',
//           'delivery_method'=>'required',
//           'payment_method'=>'required',
//           'address_one'=>'required',
//        ]);
        if (Auth::user()){
            $user_id = Auth::id();
        } else {
            $username = $this->Username($request->first_name,$request->last_name);
            $user = User::create([
                'name' => $request->first_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'username'=>$username,
                'phone'=>$request->phone,
            ]);
            $user_id = $user->id;
            Auth::loginUsingId($user_id);
        }
       $billing_info = BillingDetail::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'province'=>$request->province,
            'city'=>$request->city,
            'address_one'=>$request->address_one,
            'address_two'=>$request->address_two,
        ]);
        $cart = Cart::content();
        $subtotal = 0;
        foreach($cart as $item){
            $subtotal = $subtotal + ($item->price * $item->qty);
        }

       $order = Order::create([
          'billing_detail_id'=>$billing_info->id,
          'user_id'=>$user_id,
           'delivery_method'=>$request->delivery_method,
           'payment_method'=>$request->payment_method,
           'order_status'=> 'WFP', //Waiting for payment
           'subtotal'=>$subtotal,
           'total'=>$subtotal + 85,
           'shipping_fee'=>85
       ]);

        foreach ($cart as $item){
            OrderItem::create([
                'order_id'=>$order->id,
                'product_id'=>$item->id,
                'quantity'=>$item->qty,
                'price'=>$item->price,
                'subtotal'=>$order->subtotal
            ]);
        }

        Cart::destroy();
        $orderId = Crypt::encrypt($order->id);
        if($order->payment_method == 3 ||  $order->payment_method == 4 ){
            return back()->with(['success'=>'Order created Successfully']);
        }
        if ($order->payment_method == 2){
            return redirect('stripe-payment/'.$orderId);
//            return view('frontend.stripe.index',compact('order'));
        }
        return redirect('payment/'.$orderId)->with(['success'=>'Order created Successfully']);

    }
}
