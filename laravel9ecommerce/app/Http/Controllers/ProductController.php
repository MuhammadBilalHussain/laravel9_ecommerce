<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use Gloudemans\Shoppingcart\Facades\Cart;
//use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public $productRepo;
    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    public function index()
    {
        $products = $this->productRepo->allProduct();
        return view('product.product_list')->with(['products'=>$products]);
    }

    public function create()
    {
        return view('product.create_product');
    }

    public function store(Request $request)
    {
        $request->validate([
            'product_name'=>'required|unique:products,product_name',
            'product_price'=>'required',
            'product_quantity' =>'required',
            'product_weight' =>'required',
            'product_type' =>'required'
        ],[
            'product_name.unique'=>'This Name already Taken',
            'product_name.required'=>'This field is required'
        ]);

       $storeProduct = $this->productRepo->storeProduct($request);
       if ($storeProduct) {
           return redirect('admin/products')->with(['success' => true, 'message' => 'Product Saved Successfully']);
       }else{
           return redirect('admin/products')->with(['success' => false, 'message' => 'Something Went wrong']);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = $this->productRepo->editProduct($id);
        return view('product.edit',compact('product'));
    }


    public function update(Request $request, $id)
    {
        $product = $this->productRepo->updateProduct($request,$id);
        if ($product) {
            return redirect('admin/products')->with(['success'=>true, 'message' =>'Product update Successfully']);;
        }else{
            return redirect('admin/products')->with(['success' => false, 'message' => 'Something Went wrong']);
        }
    }

    public function destroy($id)
    {
        $this->productRepo->deleteProduct($id);
        return redirect('products')->with(['success'=>true, 'message' =>'Product Deleted Successfully']);;
    }

    public function frontendProducts(){
        $category_id = \request()->category;
        if ($category_id) {
            $products = Product::where('category_id', $category_id)->get();
        }else{
            $products = Product::all();
        }

        return view('frontend.products',compact('products'));
    }


    public function AddToCart(Request $request)
    {
        Cart::add($request->product_id,$request->product_name,1,$request->product_price,$request->product_weight);

        dd($request,Cart::Content());
    }
}
