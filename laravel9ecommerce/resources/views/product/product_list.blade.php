@extends('layouts.master')
@section('content')
    <div class="card" >
        <div class="card-header d-flex justify-content-between">
            <h4>Product list</h4>
            <a class="btn btn-primary" href="{{url('/add-product')}}">Add Product</a>
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <th>#</th>
                <th>Product Name</th>
                <th>Product Price</th>
                <th>Product Qty</th>
                <th>Product Weight</th>
                <th>Product Type</th>

                <th>Product image</th>
                <th>Action</th>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->product_name}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->quantity}}</td>
                        <td>{{$product->product_weight}}</td>
                        <td>{{$product->product_type}}</td>

                        <td>
                            @if($product->image)
                                <img src="{{asset('product_images/'.$product->image)}}" width="100px" height="50px">
                            @else
                                N/A
                            @endif
                        </td>
                        <td>

                                <a href="{{url('edit-product/'.$product->id)}}" class="btn btn-primary ">Edit</a>
                                <a href="{{url('delete-product/'.$product->id)}}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"
                                   onclick="setFormAction({{$product->id}})">Del</a>

                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="post" id="deleteForm" name="deleteForm">
                @method('DELETE')
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Modal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3>Are you sure to Delete</h3>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
    <script>
        function setFormAction(id) {
            console.log(id);
            document.deleteForm.action = "{{url('delete-product')}}" + '/' + id;
        }
    </script>


@endpush
