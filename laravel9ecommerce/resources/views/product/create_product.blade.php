@extends('layouts.master')
@section('content')

    <div class="card" style="margin-top: 100px">
        <div class="card-header">
            Create Product
        </div>
        <div class="card-body">
            {{--        {{dd($categories)}}--}}
            <form action="{{url('/save-product')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label for="product_name" >Name</label>
                        <input type="text" name="product_name" class="form-control"/>
                        @error('product_name')
                        <span class="text-danger">{{$errors->first('product_name')}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="product_price">price</label>
                        <input type="number" name="product_price" class="form-control"/>
                        @error('product_price')
                        <span class="text-danger">{{$errors->first('product_price')}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="product_quantity">Quantity</label>
                        <input type="number" name="product_quantity" class="form-control"/>
                        @error('product_quantity')
                        <span class="text-danger">{{$errors->first('product_quantity')}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="product_weight">Product Weight</label>
                        <input type="text" name="product_weight" class="form-control"/>
                        @error('product_weight')
                        <span class="text-danger">{{$errors->first('product_weight')}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="product_type">Product type</label>
                        <input type="text" name="product_type" class="form-control"/>
                        @error('product_type')
                        <span class="text-danger">{{$errors->first('product_type')}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="product_image">Image</label>
                        <input type="file" name="product_image" class="form-control" onchange="readURL(this);"/>

                        <img id="blah" src="#" style="display:none"/>

                    </div>

                </div>
                <button class="btn btn-success mt-3" >Save</button>
            </form>
        </div>
    </div>

@endsection

@push('script')
    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').css('display','block');
                    $('#blah').attr('src', e.target.result).width(100).height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endpush
