@extends('layouts.master')
@section('content')

    <div class="card" style="margin-top: 100px">
        <div class="card-header">
            Edit Product
        </div>
        <div class="card-body">
            <form action="{{url('/update-product/'.$product->id)}}" method="POST"  enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label for="product_name" >Name</label>
                        <input type="text" name="product_name" class="form-control" value="{{$product->product_name}}"/>
                    </div>
                    <div class="col-md-6">
                        <label for="product_price">price</label>
                        <input type="number" name="product_price" class="form-control" value="{{$product->price}}"/>
                    </div>
                    <div class="col-md-6">
                        <label for="product_quantity">Quantity</label>
                        <input type="number" name="product_quantity" class="form-control" value="{{$product->quantity}}"/>
                    </div>
                    <div class="col-md-6">
                        <label for="product_weight">Product Weight</label>
                        <input type="text" name="product_weight" class="form-control" value="{{$product->product_weight}}"/>
                    </div>
                    <div class="col-md-6">
                        <label for="product_type">Product Type</label>
                        <input type="text" name="product_type" class="form-control" value="{{$product->product_type}}"/>
                    </div>
                    <div class="col-md-6">
                        <label for="product_image">Image</label>
                        <input type="file" name="product_image" class="form-control" value="{{$product->image}}" onchange="readURL(this);"/>
                        @if($product->image)
                            <img id="blah" src="{{asset('product_images/'.$product->image)}}" height="50px" width="100px"/>
                        @else
                            <img id="blah" src="#" style="display:none"/>
                        @endif
                        <input type="hidden" value="{{$product->image}}" name="oldImage" />
                    </div>

                </div>
                <button class="btn btn-success mt-3" >Save</button>
            </form>
        </div>
    </div>

@endsection

@push('script')
    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').css('display','block');
                    $('#blah').attr('src', e.target.result).width(100).height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endpush
