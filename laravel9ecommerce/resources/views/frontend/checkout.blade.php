@extends('layouts.frontend_master')
@section('content')
    <div class="container">
        <!-- HERO SECTION-->
        <section class="py-1 bg-light">
            <div class="container">
                <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                    <div class="col-lg-6">
                        <h1 class="h2 text-uppercase mb-0">Checkout</h1>
                    </div>
                    <div class="col-lg-6 text-lg-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-lg-end mb-0 px-0 bg-light">
                                <li class="breadcrumb-item"><a class="text-dark"  href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item"><a class="text-dark" href="{{'cart'}}">Cart</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <!-- BILLING ADDRESS-->
            <h2 class="h5 text-uppercase mb-4">Billing details</h2>

            <div class="row">
                <div class="col-lg-8">
                    <form action="{{url('save-order')}}" method="POST">
                        @csrf
                        <div class="row gy-3">
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="firstName">First name </label>
                                <input class="form-control form-control-lg" type="text" id="firstName" placeholder="Enter your first name" name="first_name">
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="lastName">Last name </label>
                                <input class="form-control form-control-lg" type="text" id="lastName" placeholder="Enter your last name" name="last_name">
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="email">Email address </label>
                                <input class="form-control form-control-lg" type="email" id="email" placeholder="e.g. Jason@example.com" name="email" >
                            </div>
                            @if(is_null(\Illuminate\Support\Facades\Auth::user()))
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="password">Password </label>
                                <input class="form-control form-control-lg" type="password" id="password" name="password">
                            </div>
                            @endif
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="phone">Phone number </label>
                                <input class="form-control form-control-lg" type="tel" id="phone" placeholder="e.g. +02 245354745" name="phone">
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="delivery_method">Delivery Method </label>
                                <select name="delivery_method" id="delivery_method" class="form-control form-control-lg">
                                    <option>Office Pickup</option>
                                    <option>Local Shipping</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="payment_method">Payment Method </label>
                                <select name="payment_method" id="payment_method" class="form-control form-control-lg">
                                    <option value="">Select</option>
                                    @foreach($mop as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label class="form-label text-sm text-uppercase" for="province">Province</label>
                                <select class="form-control form-control-lg" id="province" name="province">
                                    <option>Choose your Province</option>
                                    <option>Punjab</option>
                                    <option>Sindh</option>
                                    <option>Blochistan</option>
                                    <option>KPK</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-label text-sm text-uppercase" for="city">Town/City </label>
                                <input class="form-control form-control-lg" type="text" id="city" name="city">
                            </div>
                            <div class="col-lg-12">
                                <label class="form-label text-sm text-uppercase" for="address_one">Address 1 </label>
                                <input class="form-control form-control-lg" type="text" id="address_one" name="address_one" placeholder="House number and street name">
                            </div>
                            <div class="col-lg-12">
                                <label class="form-label text-sm text-uppercase" for="address_two">Address 2 </label>
                                <input class="form-control form-control-lg" type="text" id="address_two" name="address_two" placeholder="Apartment, Suite, Unit, etc (optional)">
                            </div>
                            <div class="col-lg-12 form-group">
                                <button class="btn btn-dark" type="submit">Place order</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- ORDER SUMMARY-->
                <div class="col-lg-4">
                    <div class="card border-0 rounded-0 p-lg-4 bg-light">
                        <div class="card-body">
                            <h5 class="text-uppercase mb-4">Your order</h5>
                            <ul class="list-unstyled mb-0">
                                <?php $total=0; ?>
                            @foreach($cart as $item)
                                        <li class="d-flex align-items-center justify-content-between"><strong class="small fw-bold">{{$item->name}}</strong><span class="text-muted small">${{$item->qty * $item->price}}</span></li>
                                        <li class="border-bottom my-2"></li>

                                        <?php $total = $total + $item->qty * $item->price ?>
                                @endforeach
                                    <li class="d-flex align-items-center justify-content-between"><strong class="small fw-bold">Shipping Fee</strong><span class="text-muted small">$85</span></li>
                                    <li class="border-bottom my-2"></li>
                                    <li class="d-flex align-items-center justify-content-between"><strong class="text-uppercase small fw-bold">Total</strong><span>${{$total + 85}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection



