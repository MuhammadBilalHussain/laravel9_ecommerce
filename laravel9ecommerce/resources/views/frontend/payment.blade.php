@extends('layouts.frontend_master')
@section('content')
    <div class="container">
        <!-- HERO SECTION-->
        <section class="py-1 bg-light">
            <div class="container">
                <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                    <div class="col-lg-6">
                        <h1 class="h2 text-uppercase mb-0">Payment</h1>
                    </div>
                    <div class="col-lg-6 text-lg-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-lg-end mb-0 px-0 bg-light">
                                <li class="breadcrumb-item"><a class="text-dark" href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Payment</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
{{--            <h1>{{dd($order)}}</h1>--}}

            <form action="{{url('make-payment')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="order_id" value="{{$order->id}}">
                <input type="hidden" name="total" value="{{$order->total}}">
                <div class="row gy-3">
                    <div class="col-lg-6">
                        <label class="form-label text-sm text-uppercase" for="payment_method">Payment Method </label>
                        <select name="payment_method" id="payment_method" class="form-control form-control-lg">
                            <option value="">Select</option>
                            @foreach($mop as $item)
                                <option value="{{$item->id}}" {{$order->payment_method == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label class="form-label text-sm text-uppercase" for="sender_name">Sender name </label>
                        <input class="form-control form-control-lg" type="text" id="sender_name" placeholder="Enter your first name" name="sender_name">
                    </div>
                    <div class="col-lg-6">
                        <label class="form-label text-sm text-uppercase" for="paid_amount">Paid Amount (<small>Total: {{number_format($order->total)}}</small>)</label>
                        <input class="form-control form-control-lg" type="text" id="paid_amount" name="paid_amount" >
                    </div>
                    <div class="col-lg-6">
                        <label class="form-label text-sm text-uppercase" for="proof">Proof of Payment </label>
                        <input class="form-control form-control-lg" type="file" name="proof" id="proof">
                    </div>
                    <div class="col-lg-6">
                        <label class="form-label text-sm text-uppercase" for="ref_no">Ref No </label>
                        <input class="form-control form-control-lg" type="text" name="ref_no" id="ref_no">
                    </div>
                    <div class="col-lg-6">
                        <label class="form-label text-sm text-uppercase" for="sender_note">Sender Note </label>
                        <textarea name="sender_note" id="sender_note" class="form-control form-control-lg"></textarea>
                    </div>

                    <div class="col-lg-12 form-group">
                        <button class="btn btn-dark" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>


@endsection
