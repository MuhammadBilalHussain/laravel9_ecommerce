@extends('layouts.frontend_master')
@section('content')
    <section class="">
        <header>
            <h2 class="h5 text-uppercase mb-4">Top Products</h2>
        </header>
        <div class="row">
            @foreach($products as $product)
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="product text-center">
                        <div class="position-relative mb-3">
                            <div class="badge text-white bg-"></div>
                            <a class="d-block" href="#">
                                <img class="img-fluid" src="{{asset('product_images/'.$product->image)}}">
                            </a>
                            <div class="product-overlay">
                                <ul class="mb-0 list-inline">
                                    <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-outline-dark"
                                                                            href="#"><i
                                                class="far fa-heart"></i></a></li>
                                    <li class="list-inline-item m-0 p-0">
                                        <a class="btn btn-sm btn-dark" href="#" onclick="AddToCart({{$product}})">Add to cart</a></li>
                                    <li class="list-inline-item me-0">
                                        <a class="btn btn-sm btn-outline-dark"
                                           href="#productView{{$product->id}}"
                                           data-bs-toggle="modal"><i
                                                class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <h6><a class="reset-anchor" href="#">{{$product->product_name}}</a></h6>
                        <p class="small text-muted">${{$product->price}}</p>
                    </div>
                    <!--  Modal -->
                    <div class="modal fade" id="productView{{$product->id}}" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content overflow-hidden border-0">
                                <button class="btn-close p-4 position-absolute top-0 end-0 z-index-20 shadow-0"
                                        type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                <div class="modal-body p-0">
                                    <div class="row align-items-stretch">
                                        <div class="col-lg-6 p-lg-0"><a
                                                class="glightbox product-view d-block h-25 bg-center"
                                                style="background: url({{asset('product_images/'.$product->image)}}); background-repeat: no-repeat;"
                                                href="#" data-gallery="gallery1"
                                                data-glightbox="Red digital smartwatch"></a>
                                            <a class="glightbox d-none" href="#" data-gallery="gallery1"
                                               data-glightbox="Red digital smartwatch"></a>
                                            <a class="glightbox d-none" href="#"
                                               data-gallery="gallery1" data-glightbox="Red digital smartwatch"></a>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="p-4 my-md-4">
                                                <ul class="list-inline mb-2">
                                                    <li class="list-inline-item m-0"><i
                                                            class="fas fa-star small text-warning"></i></li>
                                                    <li class="list-inline-item m-0 1"><i
                                                            class="fas fa-star small text-warning"></i></li>
                                                    <li class="list-inline-item m-0 2"><i
                                                            class="fas fa-star small text-warning"></i></li>
                                                    <li class="list-inline-item m-0 3"><i
                                                            class="fas fa-star small text-warning"></i></li>
                                                    <li class="list-inline-item m-0 4"><i
                                                            class="fas fa-star small text-warning"></i></li>
                                                </ul>
                                                <h2 class="h4">Red digital smartwatch</h2>
                                                <p class="text-muted">$250</p>
                                                <p class="text-sm mb-4">Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum
                                                    sociis natoque penatibus et magnis dis parturient montes nascetur
                                                    ridiculus mus. Vestibulum ultricies aliquam convallis.</p>
                                                <div class="row align-items-stretch mb-4 gx-0">
                                                    <div class="col-sm-7">
                                                        <div
                                                            class="border d-flex align-items-center justify-content-between py-1 px-3">
                                                            <span class="small text-uppercase text-gray mr-4 no-select">Quantity</span>
                                                            <div class="quantity">
                                                                <button class="dec-btn p-0"><i
                                                                        class="fas fa-caret-left"></i></button>
                                                                <input class="form-control border-0 shadow-0 p-0"
                                                                       type="text" value="1">
                                                                <button class="inc-btn p-0"><i
                                                                        class="fas fa-caret-right"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5"><a
                                                            class="btn btn-dark btn-sm w-100 h-100 d-flex align-items-center justify-content-center px-0"
                                                            href="">Add to cart</a></div>
                                                </div>
                                                <a class="btn btn-link text-dark text-decoration-none p-0" href="#!"><i
                                                        class="far fa-heart me-2"></i>Add to wish list</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
@push('js')
    <script>
        function AddToCart(product) {
            let product_name = product.product_name;
            let product_price = product.price;
            let product_weight = product.product_weight;
            // let product_image = product.product_name;
            let product_id = product.id;
            let formData = new FormData();
            let image = product.image;

            formData.append('_token',"{{csrf_token()}}");
            formData.append('product_name',product_name);
            formData.append('product_price',product_price);
            formData.append('product_weight',product_weight);
            formData.append('product_id',product_id);
            formData.append('image',image);


            $.ajax({
                url: "{{url("add-to-cart")}}",
                contentType: false,
                cache: false,
                processData: false,
                type: 'POST',
                data: formData,
                // success: function (response) {
                //     $('#cartQty').text(response.cart_items);
                //     alert(response.message);
                // }
                success: function (response) {
                    swal.fire('success', response.message, 'success');
                    location.reload();
                }
            })

        }
    </script>
@endpush
