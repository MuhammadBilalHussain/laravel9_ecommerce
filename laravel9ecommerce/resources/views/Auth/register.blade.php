@extends('layouts.frontend_master')
@section('content')

    <div class="wrapper w-50 mt-5">
        <div class="card">
            <div class="card-header">
                Register
            </div>
            <div class="card-body">
                <form action="{{route('saveUser')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <label for="name">First Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="first_name">
                            @error('name')
                            <span class="text-danger">{{$errors->first('first_name')}}</span>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <label for="name">Last Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="last_name">
                            @error('name')
                            <span class="text-danger">{{$errors->first('last_name')}}</span>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" name="email">
                            @error('email')
                            <span class="text-danger">{{$errors->first('email')}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="form-control" placeholder="Enter Phone" name="phone">
                            @error('email')
                            <span class="text-danger">{{$errors->first('phone')}}</span>
                            @enderror
                        </div>
                        <div class="col-md-12 mt-2">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" placeholder="Enter Password" name="password">
                            @error('password')
                            <span class="text-danger">{{$errors->first('password')}}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <button class="btn btn-success mt-2">Register</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
