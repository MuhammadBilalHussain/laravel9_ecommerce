<div class="sidebar-panel">
    <div class="gull-brand pr-3 text-center mt-4 mb-2 d-flex justify-content-center align-items-center"><img class="pl-3" src="{{asset('assets/images/logo.png')}}" alt="alt" />
        <!--  <span class=" item-name text-20 text-primary font-weight-700">GULL</span> -->
        <div class="sidebar-compact-switch ml-auto"><span></span></div>
    </div>
    <!--  user -->
    <div class="scroll-nav ps ps--active-y" data-perfect-scrollbar="data-perfect-scrollbar" data-suppress-scroll-x="true">
        <div class="side-nav">
            <div class="main-menu">
                <ul class="metismenu" id="menu">
                    <li class="Ul_li--hover"><a  href="{{url('/dashboard')}}"><i class="i-Bar-Chart text-20 mr-2 text-muted"></i><span class="item-name text-15 text-muted">Dashboard</span></a></li>
                    <li class="Ul_li--hover"><a href="{{url('/admin/products')}}"><i class="i-File-Horizontal-Text text-20 mr-2 text-muted"></i><span class="item-name text-15 text-muted">Products</span></a></li>
                    <li class="Ul_li--hover"><a href="{{url('/admin/payment-verification')}}"><i class="i-File-Horizontal-Text text-20 mr-2 text-muted"></i><span class="item-name text-15 text-muted">Payment Verifications</span></a></li>
                    <li class="Ul_li--hover"><a class="has-arrow" href="#" aria-expanded="false"><i class="i-Bar-Chart text-20 mr-2 text-muted"></i><span class="item-name text-15 text-muted">Order Fullfilment</span></a>
                        <ul class="mm-collapse" style="height: 0px;">
                            <li class="item-name"><a href="{{url('/admin/office-pickups')}}"><i class="i-Circular-Point mr-2 text-muted"></i><span class="text-muted">Office Pickup</span></a></li>
                            <li class="item-name"><a href="{{url('/admin/wfp')}}"><i class="i-Circular-Point mr-2 text-muted"></i><span class="text-muted">WFP</span></a></li>
                            <li class="item-name"><a href="{{url('/admin/rfa')}}"><i class="i-Circular-Point mr-2 text-muted"></i><span class="text-muted">RFA </span></a></li>
                            <li class="item-name"><a href="{{url('/admin/afs')}}"><i class="i-Circular-Point mr-2 text-muted"></i><span class="text-muted">AFS</span></a></li>
                            <li class="item-name"><a href="{{url('/admin/del')}}"><i class="i-Circular-Point mr-2 text-muted"></i><span class="text-muted">DEL</span></a></li>
                        </ul>
                    </li>
                    </li>
                </ul>
            </div>
        </div>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 404px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 325px;"></div>
        </div>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 404px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 325px;"></div>
        </div>
    </div>
    <!--  side-nav-close -->
</div>
