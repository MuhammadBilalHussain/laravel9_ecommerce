@if($errors->any())
    <div class="alert alert-danger error-alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success error-alert">
        {{session()->get('success')}}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger error-alert">
        {{session()->get('error')}}
    </div>
@endif

<script>
    $("document").ready(function() {
        setTimeout(function () {
            $('.error-alert').remove();
        }, 3000);
    });
</script>
