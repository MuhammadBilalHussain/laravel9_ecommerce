@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-header">
                    <button class="btn btn-danger btn-sm float-left"> OOH</button>
                    <a href="{{url('admin/make-AFS/'.$order->id)}}" class="btn btn-success btn-sm float-right"> Make AFS</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form id="invoice" class="">
                                <div class="z-index-50 position-relative">
                                <!-- Header Start Here -->
                                    <header class="d-flex justify-content-between mb-5">
                                        <div class="logo">
                                            <img alt="Logo" src="{{asset('assets/images/logo.png')}}"
                                                class="companyName" width="100px"/>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <div class="d-flex justify-content-between mr-2">
                                                <span class="icon ml-1">
                                                    <i
                                                        class="fa fa-map-marker"
                                                        aria-hidden="true"
                                                    ></i>
                                                </span>
                                                <p>
                                                    Blk 1 19, Pandan, Angeles
                                                    City, Pampanga
                                                </p>
                                            </div>
                                            <div class="d-flex justify-content-between ml-2">
                                                <span class="icon mr-1">
                                                    <i
                                                        class="fa fa-phone"
                                                        aria-hidden="true"
                                                    ></i>
                                                </span>
                                                <p>09176317577</p>
                                            </div>
                                        </div>
                                    </header>
                                    <!-- Header End Here -->

                                    <!-- Section 1 Start Here -->
                                    <div class="mainSection">
                                        <section class="subSection">
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <h1 class="heading font-weight-bold">
                                                        {{ $order->BillingInfo->first_name ?? 'N/A' }}
                                                        {{ $order->BillingInfo->last_name ?? 'N/A' }}
                                                    </h1>
                                                    <p class="subheading">
                                                        ({{ $order->User ? $order->User->name : '' }})
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="mt-4">
                                                <ul class="contact-info">
                                                    <li>
                                                        <span class="icon">
                                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                                        </span>
                                                        <p style="font-size: 16px;">
                                                            {{ @$order->User->phone ?? 'N/A' }}
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <span class="icon">
                                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        </span>
                                                        <p style="font-size: 16px;">
                                                            {{ @$order->User->email ?? 'N/A' }}
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <span class="icon">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        </span>
                                                        <p style="font-size: 16px;">
                                                            {{ @$order->BillingInfo->address_one ?? 'N/A' }}
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </section>
                                        <!-- Right Section -->
                                        <section class="subSection">
                                            <h1 style="
                                            font-weight: bold;
                                        ">Order Detail</h1>
                                            <div class="line"></div>
                                            <div class="orderDetail mt-2">
                                                <ul>
                                                    <li>
                                                        <h1
                                                            style="
                                                                font-weight: bold;
                                                            "
                                                        >
                                                            Order#
                                                            {{ $order->id }}
                                                        </h1>
                                                    </li>
                                                    <li>
                                                        Order Date:
                                                        {{ $order->created_at }}
                                                    </li>
                                                    <li>
                                                        Order Status:
                                                        {{ ucwords($order->order_status) }}
                                                    </li>
                                                    <li>
                                                        MOP:
                                                        {{ $order->PaymentMethod->name ?? 'N/A' }}
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        Total Due :₱
                                                        {{ $order->total ?? 'N/A' }}
                                                    </li>
                                                    <li>
                                                        Branch : FSD
                                                    </li>
                                                </ul>
                                            </div>
                                        </section>
                                    </div>
                                    <!-- Section 1 End Here -->

                                    <!-- Section 3 Start Here -->
                                    <section class="productTable">
                                        <div class="table-responsive mb-5">
                                            <table class="table table-borderless">
                                                <thead>
                                                <tr>
                                                    <td>Product Code</td>
                                                    <td>Photo</td>
                                                    <td class="text-left">
                                                        Item Name
                                                    </td>
                                                    <td>Price</td>
                                                    <td>Quantity</td>
                                                    <td>Total</td>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php $discount = 0; ?>
                                                @foreach($order->OrderItems as $key => $item)
                                                    <tr>
                                                        <td style="font-size: 16px;"> {{$item->Product->id}}</td>
                                                        <td>
                                                            <div class="product-img">
                                                                <img src="{{asset('product_images'.@$item->Product->image)}}"
                                                                     alt="Product Image"/>
                                                            </div>
                                                        </td>
                                                        <td class="text-left description">
                                                            <div>
                                                                <p class="text-break"
                                                                   style="font-size: 16px;">{{$item->Product->name}}</p>
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 16px;">
                                                            ₱{{$item->price}}
                                                        </td>
                                                        <td style="font-size: 16px;">
                                                            {{$item->quantity}}
                                                        </td>
                                                        <td style="font-size: 16px;">
                                                            ₱{{$item->subtotal}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr class="border-top border-bottom">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="font-size: 16px;">
                                                        ₱ {{$order->orderItems->sum('price')}}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                        <div class="tableRight">
                                            <section class="checkoutTable">
                                                <div>
                                                    <div>Subtotal :</div>
                                                    <div>
                                                        ₱{{ $order->subtotal }}
                                                    </div>
                                                </div>
                                                <div>
                                                    <div>Shipping Fee :</div>
                                                    <div>
                                                        ₱
                                                        {{ $order->shipping_fee ?? 0 }}
                                                    </div>
                                                </div>
                                                <div class="filled">
                                                    <div>Total :</div>
                                                    <div>
                                                        ₱ {{ $order->total }}
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </section>
                                    <!-- Section 3 End Here -->
                                </div>
                            </form>
                        </div>
                        <hr class="line"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
