@extends('layouts.master')
@section('content')

    <div class="card" style="margin-top: 100px">
        <div class="card-header">
            Payment List
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill w-50 mb-3" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pending</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Approve</a>
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">decline</a>
                        </div>
                    </nav>
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order No</th>
                                    <th>Payment Method</th>
                                    <th>Sender Name</th>
                                    <th>Reference No</th>
                                    <th>Payment Note</th>
                                    <th>Payment date</th>
                                    <th>Payment Proof</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($payments) > 0)
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{$payment->id}}</td>
                                        <td>{{$payment->order_id}}</td>
                                        <td>{{$payment->PaymentMethod->name}}</td>
                                        <td>{{$payment->sender_name}}</td>
                                        <td>{{$payment->ref_no}}</td>
                                        <td>{{$payment->sender_note}}</td>
                                        <td>{{$payment->date}}</td>
                                        <td class="parent-container">
                                            <a href="{{asset('payment_proofs/'.$payment->proof)}}" target="_blank">
                                                <img src="{{asset('payment_proofs/'.$payment->proof)}}" alt="payment_proof" width="50px">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{url('admin/verify-payment/'.encrypt($payment->id))}}" class="btn btn-success">
                                                Approve
                                            </a>
                                            <a href="{{url('admin/decline-payment/'.encrypt($payment->id))}}" class="btn btn-danger">
                                                decline
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                @else

                                    <tr class="text-center">
                                        <td colspan="7">
                                            No Data Found
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order No</th>
                                    <th>Payment Method</th>
                                    <th>Sender Name</th>
                                    <th>Reference No</th>
                                    <th>Payment Note</th>
                                    <th>Payment date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($payments_approved) > 0)
                                @foreach($payments_approved as $payment)
                                    <tr>
                                        <td>{{$payment->id}}</td>
                                        <td>{{$payment->order_id}}</td>
                                        <td>{{$payment->PaymentMethod->name}}</td>
                                        <td>{{$payment->sender_name}}</td>
                                        <td>{{$payment->ref_no}}</td>
                                        <td>{{$payment->sender_note}}</td>
                                        <td>{{$payment->date}}</td>
                                    </tr>
                                @endforeach

                                @else
                                    <tr class="text-center">
                                        <td colspan="7">
                                            No Data Found
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order No</th>
                                    <th>Payment Method</th>
                                    <th>Sender Name</th>
                                    <th>Reference No</th>
                                    <th>Payment Note</th>
                                    <th>Payment date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($payments_decline) > 0)
                                @foreach($payments_decline as $payment)
                                    <tr>
                                        <td>{{$payment->id}}</td>
                                        <td>{{$payment->order_id}}</td>
                                        <td>{{$payment->PaymentMethod->name}}</td>
                                        <td>{{$payment->sender_name}}</td>
                                        <td>{{$payment->ref_no}}</td>
                                        <td>{{$payment->sender_note}}</td>
                                        <td>{{$payment->date}}</td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr class="text-center">
                                        <td colspan="7">No data found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

