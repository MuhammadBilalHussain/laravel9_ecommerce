@extends('layouts.master')

@section('content')

<div class="card">
    <div class="card-body">
        <table class="display table table-striped table-bordered" id="zero_configuration_table" >
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Payment Method</th>
                <th>Delivery Method</th>
                <th>Order Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as  $order)

                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->PaymentMethod->name}}</td>
                    <td>{{$order->delivery_method}}</td>
                    <td>{{$order->order_status}}</td>
                    <td>
                        <a href="{{url('admin/mark-as-paid/'.encrypt($order->id))}}"  title="Approve" class="btn btn-sm btn-success">
                            Mark as Paid
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>


@endsection
