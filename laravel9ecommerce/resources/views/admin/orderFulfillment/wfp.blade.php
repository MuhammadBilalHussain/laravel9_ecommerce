@extends('layouts.master')
@section('content')

    <div class="card">

        <div class="card-body">
            <table class="display table table-striped table-bordered" id="zero_configuration_table" >
                <thead>
                <tr>
                    <th>Order ID</th>
                    {{--                    <th>Sender Name</th>--}}
                    <th>Payment Method</th>
                    <th>Delivery Method</th>
                    {{--                    <th>Paid Amount</th>--}}
                    <th>Order Status</th>
                    {{--                    <th>Payment Note</th>--}}
                    {{--                    <th>Reference Number</th>--}}
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as  $order)

                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->PaymentMethod->name}}</td>
                        <td>{{$order->delivery_method}}</td>
                        <td>{{$order->order_status}}</td>



                        <td>
                            <a href=""  title="Approve" class="btn btn-sm btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i>

                            </a>
                            <a href="" title="Decline" class="btn btn-sm btn-danger">
                                <i class="fa fa-ban" aria-hidden="true"></i>

                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
