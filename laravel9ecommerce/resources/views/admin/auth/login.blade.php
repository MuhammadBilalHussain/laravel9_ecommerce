@extends('layouts.master')
@section('content')

    <div class="card w-25 ml-auto mr-auto mt-5" >
        <div class="card-header">
           Admin Login
        </div>
        <div class="card-body" >

            @if(session()->has('error'))
                <div class="alert alert-danger error-alert">
                    {{session()->get('error')}}
                </div>
            @endif
            <form action="{{url('admin-login')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label for="email">Email</label>
                        <input type="email" name="email" placeholder="Email" class="form-control" id="email">
                    </div>
                    <div class="col-md-12">
                        <label for="password">Password</label>
                        <input type="password" name="password" placeholder="Password" class="form-control" id="password">
                    </div>
                    <div class="col-md-12 text-right mt-3">
                        <button class="btn btn-primary ml-auto">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
