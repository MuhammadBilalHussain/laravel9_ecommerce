<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'name'=>'Clothes',
                'category_image'=>'1675352363.jpg'
            ],
            [
                'name'=>'Shoes',
                'category_image'=>'1675352865.jpg'
            ],
            [
                'name'=>'Watches',
                'category_image'=>'1675353633.jpg'
            ],
            [
                'name'=>'Electronics',
                'category_image'=>'1675369010.jpg'
            ],

        ];

        Category::insert($arr);
    }
}
