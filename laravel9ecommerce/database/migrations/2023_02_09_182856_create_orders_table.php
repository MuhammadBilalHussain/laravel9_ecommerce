<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('billing_detail_id')->nullable();
            $table->text('payment_method')->nullable();
            $table->text('delivery_method')->nullable(); // office-pickup, local shipping, international
            $table->enum('order_status',['WFP','RFA','AFS','CXO','DEL']);
            $table->double('subtotal')->nullable();
            $table->double('shipping_fee')->nullable();
            $table->double('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
