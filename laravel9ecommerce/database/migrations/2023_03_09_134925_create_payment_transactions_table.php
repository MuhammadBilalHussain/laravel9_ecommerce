<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('payment_method')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('bank_detail_id')->nullable();
            $table->double('total')->nullable();
            $table->double('paid_amount')->nullable();
            $table->text('sender_note')->nullable();
            $table->date('verification_date')->nullable();
            $table->date('date')->nullable();
            $table->date('deleted_at')->nullable();
            $table->text('details')->nullable();
            $table->enum('order_status',['WFP','RFA','AFS','CXO','DEL']);
            $table->string('proof')->nullable();
            $table->string('ref_no')->nullable();
            $table->string('sender_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
};
