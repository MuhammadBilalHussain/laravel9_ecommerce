<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('payment_method')->nullable();
            $table->string('sender_name')->nullable();
            $table->date('date')->nullable();
            $table->double('total')->nullable();
            $table->double('paid_amount')->nullable();
            $table->string('proof')->nullable();
            $table->text('sender_note')->nullable();
            $table->string('ref_no')->nullable();
            $table->date('verification_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
